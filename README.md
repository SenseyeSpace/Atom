# Atom

##### Issues by multi languages
|                                                               | C                                                           | Rust                                                           | Golang                                                            | Swift | Python                                                           | C#                                                      | JavaScript                                                           |
|---------------------------------------------------------------|-------------------------------------------------------------|----------------------------------------------------------------|-------------------------------------------------------------------|-------|------------------------------------------------------------------|---------------------------------------------------------|----------------------------------------------------------------------|
| Palindrome made from the product of two 5-digit prime numbers | [+](https://gitlab.com/SenseyeSpace/PrimeNumberPalindromeC) | [+](https://gitlab.com/SenseyeSpace/PrimeNumberPalindromeRust) | [+](https://gitlab.com/SenseyeSpace/PrimeNumberPalindromeGolang)  |       | [+](https://gitlab.com/SenseyeSpace/PrimeNumberPalindromePython) |                                                         | [+](https://gitlab.com/SenseyeSpace/PrimeNumberPalindromeJavaScript) |
| Tower of Hanoi                                                |                                                             | [+](https://gitlab.com/SenseyeSpace/TowerOfHanoiRust)          | [+](https://gitlab.com/SenseyeSpace/TowerOfHanoiGolang)           |       |                                                                  | [+](https://gitlab.com/SenseyeSpace/TowerOfHanoiCSharp) |                                                                      |
| First ten thousand prime number                               |                                                             |                                                                | [+](https://gitlab.com/SenseyeSpace/FirstTenThousandPrimeNumbers) |       |                                                                  |                                                         |                                                                      |

##### Training Resources:
* [Codewars: Train with Programming Challenges/Kata](https://www.codewars.com/)
* [LeetCode — The World's Leading Online Programming Learning Platform](https://leetcode.com/)
* [Python Tutor](http://www.pythontutor.com/)

##### IDEs:
* 3D map of Language, Platform, IDE name
